#! /bin/bash
# md2pdf.sh
# Convert Markdown file to PDF

MARKDOWN_PROJECT_PATH=$(dirname $0)

USAGE="usage: $0 mdFilePath [outDirectory]"

[[ -z ${1} ]] && echo ${USAGE} >&2 && exit 1
MD_FILE=${1}

OUT_DIR_OPTION="-o $PWD"
[[ -n ${2} ]] && OUT_DIR_OPTION="-o ${2}"

gimli -f ${MD_FILE} -s ${MARKDOWN_PROJECT_PATH}/style.css -w '-t ' ${OUT_DIR_OPTION}

exit 0

