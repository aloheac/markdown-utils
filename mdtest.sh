#! /bin/bash
# mdtest.sh
# Convert Markdown file to PDF and remove it immediately after

MARKDOWN_PROJECT_PATH=$(dirname $0)

USAGE="usage: $0 mdFilePath"

[[ -z ${1} ]] && echo ${USAGE} >&2 && exit 1
MD_FILE_PATH=${1}
MD_FILE_NAME=$(basename ${MD_FILE_PATH})

OUT_DIR=$(mktemp -d)
OUT_FILE_NAME=${MD_FILE_NAME/%.md/.pdf}

${MARKDOWN_PROJECT_PATH}/md2pdf.sh ${MD_FILE_PATH} ${OUT_DIR}
evince ${OUT_DIR}/${OUT_FILE_NAME}

# Clean temp files
rm ${OUT_DIR}/${OUT_FILE_NAME}
rmdir ${OUT_DIR}

exit 0
