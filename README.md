# Mardown utils

## Presentation

`markdown-utils` allow user to convert a Markdown file to PDF with a default
configuration.

## Configuration

Configure your environment with the following commands:

```bash
sudo apt-get install rubygems-integration wkhtmltopdf ruby-dev
sudo gem install gimli
```

## Usage

### Configuration

You can add aliases to the scripts in your ~/.bash_aliases:

```bash
alias mdtest='<path to markdown-utils>/mdtest.sh'
alias md2pdf='<path to markdown-utils>/md2pdf.sh'
```

### Convert a file

`md2pdf <md file path>`

The PDF will be created to the current directory

### Test a file

Use this to see a PDF file without keeping it

`mdtest <md file path>`

The PDF file will be deleted when the PDF viewer is closed

